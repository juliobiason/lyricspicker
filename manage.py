#!/usr/bin/env python
# -*- encoding: utf-8 -*-


# before everything else, start the logging system in DEBUG mode for the
# manager. this way we can log everything, including the app start up
import logging
if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG,
                        format='%(levelname)s:%(name)s '
                        '[%(funcName)s:%(lineno)d]:%(message)s')

# and now back to your normal schedule
from flask.ext.script import Manager
from flask.ext.script import Server

from lyrics import app

manager = Manager(app)

manager.add_command('runserver', Server(use_debugger=True,
                                        use_reloader=True))

if __name__ == '__main__':
    manager.run()
