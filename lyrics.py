#!/usr/bin/env python
# -*- encoding: utf-8 -*-


import yaml
import random


def work(message):
    print u'>>>', message, u'...'


def title(message):
    print u"\n{0}\n".format(message)


def display(message):
    print message


def get_songs():
    work(u'Getting content')
    result = None
    with open('lyrics.yaml') as content:
        result = yaml.load(content)
    return result


def pick_song(songs):
    work(u'Picking a song')
    song = random.choice(songs)

    while song['done']:
        work(u'Song "{0}" was already picked, getting a new '
             u'one'.format(song['song']))
        song = random.choice(songs)

    return song


def firstialize(string):
    result = []
    for word in string.split(' '):
        result.append(word[0])
    return ''.join(result).upper()


def show_hints(song):
    display(u'Artist: {0}'.format(firstialize(song['artist'])))
    display(u'Song: {0}'.format(firstialize(song['song'])))


def show_info(song):
    display(u'Artist: {0}'.format(song['artist']))
    display(u'Song: {0}'.format(song['song']))


def show_song(song):
    title(u'Song picked, here it is:')
    display(song['lyrics'])
    display(u'')

    show_hints(song)

    raw_input(u'Press enter to display the answer:')

    show_info(song)


def main():
    songs = get_songs()
    while True:
        song = pick_song(songs)
        show_song(song)

if __name__ == '__main__':
    main()
