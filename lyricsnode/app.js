
/**
 * Module dependencies.
 */

var express = require('express');
var round = require('./routes/round');
var database = require('./routes/database');
var http = require('http');
var path = require('path');
var cons = require('consolidate');
var swig = require('swig');

var app = express();

// all environments
app.engine('.html', cons.swig);
swig.init({
  root: path.join(__dirname, 'views'),
  cache: false,
	allowErrors: true
});

app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use('/static', express.static(path.join(__dirname, 'public')));

// development only
if ('development' === app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', round.index);
app.post('/round/others/', round.others);
app.post('/round/played/', round.played);

app.get('/db/', database.index);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
