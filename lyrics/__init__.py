#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from flask import Flask

import logging

# ----------------------------------------------------------------------
# Start up
# ----------------------------------------------------------------------

_log = logging.getLogger('lyrics')

app = Flask(__name__)
app.config['SIDEBAR'] = []

# ----------------------------------------------------------------------
# Import and register blueprints
# ----------------------------------------------------------------------

from lyrics.blueprints.index import index

blueprints = [(index, '/', 'Index')]

for (blueprint, path, title) in blueprints:
    _log.debug('{blueprint} => "{path}" as "{title}"'.format(
        blueprint=blueprint, path=path, title=title))
    app.register_blueprint(blueprint, url_prefix=path)
    app.config['SIDEBAR'].append((title, path))

# ----------------------------------------------------------------------
# Start as application
# ----------------------------------------------------------------------
if __name__ == '__main__':
    app.run()
