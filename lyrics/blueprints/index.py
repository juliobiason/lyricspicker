#!/usr/bin/env python
# -*- encoding: utf-8 -*-

from flask import Blueprint
from flask import render_template

index = Blueprint('index', __name__)

@index.route('/')
def root():
    return render_template('index.tpl')
