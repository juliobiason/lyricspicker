#!/usr/bin/env python
# -*- encoding: utf-8 -*-

"""Database models and functions."""

from datetime import date
from pony.orm import *

import inspect
import os.path

file_path = inspect.getsourcefile(self)
file_path = os.path.dirname(file_path)

db = Database("sqlite",
              os.path.join(file_path, "lyrics.sqlite"),
              create_db=True)

class Artist(db.Entity):
    name = Required(unicode)
    songs = Set("Song")

class Song(db.Entity):
    artists = Set(Artist)
    name = Required(unicode)
    done = Optional(date)
    youtube = Optional(unicode)
    lyric = Required("Lyric")

class Lyric(db.Entity):
    text = Required(LongUnicode)
    song = Required(Song)
